package distsys;


import java.sql.*;

public class Row implements java.io.Serializable {
    
    private static final long serialVersionUID = 42L;
    private int id;
    private String POI;
    private double Lat;
    private double Long;
    private String Photos;
    
    public Row(ResultSet rs){
        try{
        id = rs.getInt(1);
        POI=rs.getString(3);
        Lat=(double)Math.round(rs.getDouble(7)*1000)/1000;
        Long = (double)Math.round(rs.getDouble(8)*1000)/1000;
        Photos = rs.getString(10);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public int getID() {
        return id;
    }

 
    public String getPOI() {
        return POI;
    }

    public void setPOI(String POI) {
        this.POI = POI;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double Lat) {
        this.Lat = Lat;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double Long) {
        this.Long = Long;
    }

    public String getPhotos() {
        return Photos;
    }

    public void setPhotos(String Photos) {
        this.Photos = Photos;
    }
    
    public Row getSelf(){
        return this;
    }
    
    public LL getLL(){
        return new LL(Lat,Long);
    }
}
