package distsys;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.omg.CORBA.PERSIST_STORE;

public class MapWorker {

    private InetAddress ReducerIP;
    private int srv_port, rd_port;
    private int tN;
    private String user = "omada61", pass = "omada61db";
    private ArrayList<Row> taskList;
    private int State;
    private ServerSocket ssock;
    private Socket sock;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private MapTask m = null;
    HashMap<String, ArrayList<LL>> LLMap;

    public MapWorker(String rd_IP, int srv_port, int rd_port) {
        try {
            this.ReducerIP = InetAddress.getByName(rd_IP);

        } catch (Exception e) {
            e.printStackTrace();
        }
        this.srv_port = srv_port;
        this.rd_port = rd_port;
        tN = Runtime.getRuntime().availableProcessors();
        System.out.println("Will use " + tN + "Threads");
        try {
            ssock = new ServerSocket(srv_port);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public void waitForTasksThread() {
        System.out.println("Waiting for Tasks at port " + srv_port + " ... ");
        try {
            sock = ssock.accept();
            in = new ObjectInputStream(sock.getInputStream());
            out = new ObjectOutputStream(sock.getOutputStream());

            m = (MapTask) in.readObject();
            System.out.println(m.toString());
            System.out.println("\nReceived : " + m.toString());
            Map<LL, Integer> mapList = map(m.getT_min(), m.getT_max());
            sendToReducers(mapList);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //100%
    public void notifyMaster() {
        try {
            out.writeInt(State);
            out.flush();
            System.out.println("Finished Task and notified Master...");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //100%
    public void sendToReducers(Map<LL, Integer> m) {
        System.out.println("Sending to Reducer ... ");

        try {
            Socket sc = new Socket(ReducerIP, rd_port);
            ObjectOutputStream outl = new ObjectOutputStream(sc.getOutputStream());
            if (!m.isEmpty()) {
                outl.writeObject(m);
            } else {
                outl.writeObject(null);
            }
            System.out.println(m.size());
            outl.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        State = 1;
        notifyMaster();
    }

    //100%
    ArrayList<Row> fetchFromDatabase(Timestamp from, Timestamp to) {
        ArrayList<Row> r = new ArrayList<Row>();
        String url;
        url = "jdbc:mysql://83.212.117.76:3306/";
        String driver = "com.mysql.jdbc.Driver";
        String Query = "SELECT * FROM ds_systems_2016.checkins where latitude >= " + m.getLat1() + " and latitude <= " + m.getLat2()
                + " and longitude >= " + m.getLong1() + " and longitude <= " + m.getLong2() + " and `time` between '" + from.toString() + "' and '" + to.toString() + "'";

        //System.out.println("query = " + Query);
        try {
            Class.forName(driver).newInstance();
            ResultSet res;
            try (Connection conn = DriverManager.getConnection(url, user, pass)) {
                Statement st = conn.createStatement();
                res = st.executeQuery(Query);
                while (res.next()) {
                    r.add(new Row(res));
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
            System.out.println("Will process" + r.size() + " results");
            return r;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    //100%
    public Map<LL, Integer> map(Timestamp from, Timestamp to) {

        this.taskList = fetchFromDatabase(from, to);
        createPOILL();
        Map<LL, Integer> m1 = taskList.parallelStream()
                .filter(f->(!f.getPhotos().equalsIgnoreCase("Not exists")))
                .collect(Collectors.groupingBy(Row::getLL, Collectors.collectingAndThen(Collectors.mapping(Row::getPhotos, Collectors.toSet()), Set::size)));
        return m1;
    }
    
    
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
    
   
    public void createPOILL() {
        LLMap = new HashMap<>();
        taskList.stream().forEach((r) -> {
            if (LLMap.containsKey(r.getPOI())) {
                LLMap.get(r.getPOI()).add(r.getLL());
            } else {
                ArrayList<LL> tmpl = new ArrayList<>();
                tmpl.add(r.getLL());
                LLMap.put(r.getPOI(), tmpl);
            }
        });
        ArrayList<Row> tmp = new ArrayList<>();
        for (int i = 0; i < taskList.size(); i++) {
            tmp.add(taskList.get(i));
            LL l = LLMap.get(taskList.get(i).getPOI()).get(0);
            tmp.get(i).setLat(l.getLat());
            tmp.get(i).setLong(l.getLong());
        }
        taskList.clear();
        taskList.addAll(tmp);
    }

    public void close() {
        try {
            ssock.close();
            sock.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        Scanner s;
        try {
            //s = new Scanner(System.in);
            //System.out.println("Type reducer_ip :");
            //String reducer_ip = s.nextLine();
            //MapWorker worker = new MapWorker(reducer_ip, 3000, 4000);

            MapWorker worker = new MapWorker("127.0.0.1", 3000, 4000);
            while (true) {
                worker.waitForTasksThread();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
