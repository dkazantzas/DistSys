package distsys;


import java.util.Map;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;

public class ReducerThread implements Runnable {

    private Socket sock;

    private Map<LL, Integer> m;

    public ReducerThread(Socket sock) {
        this.sock = sock;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        try {
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            m = (Map<LL, Integer>) in.readObject();
            System.out.println("Received results from "+sock.getInetAddress());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Map<LL, Integer> call() throws Exception {
        return m;
    }

}
