package distsys;


import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class mapThread implements Callable<Map<LL, Integer>> {
    private HashMap<String,ArrayList<LL>> LLMap;
    private final ArrayList<Row> taskList;

    public mapThread(ArrayList<Row> taskList) {
        this.taskList = taskList;
    }

    @Override
    public Map<LL, Integer> call() throws Exception {
        Map<String,Integer> m1 = taskList.stream()
                .filter(f->(!f.getPhotos().equalsIgnoreCase("Not exists")))
                .collect(Collectors.groupingBy(Row::getPOI, Collectors.collectingAndThen(Collectors.mapping(Row::getPhotos, Collectors.toSet()), Set::size)));
        Map<LL,Integer> m2 = new HashMap<>();
        createPOILL();
        m1.keySet().stream().forEach((s) -> {
            m2.put(LLMap.get(s).get(0), m1.get(s));
        });
        return m2;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
    
    public void createPOILL(){
        LLMap = new HashMap<>();
        taskList.stream().forEach((r) -> {
            if(LLMap.get(r.getPOI())!=null){
                LLMap.get(r.getPOI()).add(r.getLL());
            }else{
                ArrayList<LL> tmpl = new ArrayList<>();
                tmpl.add(r.getLL());
                LLMap.put(r.getPOI(), tmpl);
            }
        });
    }
}
