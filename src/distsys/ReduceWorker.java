package distsys;

import java.io.*;
import java.util.*;
import java.net.*;
import java.util.stream.Collectors;

public class ReduceWorker {

    private int l = 10;
    private Socket msock, csock;
    private ServerSocket mssock, cssock;
    private ObjectInputStream cin;
    private ObjectOutputStream cout;
    private Map<LL, Integer> rtask, mtask;
    private final int cport, mport;
    private final int mapN;


	public void reduce() {
        System.out.println("Starting Reduction");
        rtask = mtask
                .entrySet()
                .stream()
                .sorted(Map.Entry.<LL, Integer>comparingByValue().reversed())
                .limit(l)
                .collect(
                        Collectors
                        .toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public void sendResults() {
        try {
            cout.writeObject(rtask);
            cout.flush();
            System.out.println("Sent" + l +" results");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void waitForTasksThread() {
        System.out.println("Waiting for tasks at port " + mport);
        try {
            mssock = new ServerSocket(mport);        
            cssock = new ServerSocket(cport);
            Thread cThread = new Thread(new ClientWatcher(cssock));
            cThread.start();
            Thread t[] = new Thread[mapN];
            ReducerThread rt[] = new ReducerThread[mapN];
            for (int i = 0; i < mapN; i++) {
                msock = mssock.accept();
                rt[i] = new ReducerThread(msock);
                t[i] = new Thread(rt[i]);
                t[i].start();
            }
            mtask = new HashMap<>();
            for (int i = 0; i < mapN; i++) {
                try {
                    t[i].join();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            for (int i = 0; i < mapN; i++) {
                try {
                    mtask.putAll(rt[i].call());
                }catch(NullPointerException e){
                
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void close() {
        try {
            cssock.close();
            csock.close();
            msock.close();
            cin.close();
            cout.close();
            mssock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ReduceWorker(int l, int cport, int mport, int mapN) {
        super();
        this.l = l;
        this.cport = cport;
        this.mport = mport;
        this.mapN = mapN;
    }

    public static void main(String[] args) {
        //Scanner sc = new Scanner(System.in);
        /*System.out.println("Enter Port for incoming client connections : ");
        int cport = Integer.parseInt(sc.nextLine());
        System.out.println("Enter Port for incoming mapper connections : ");
        int mport = Integer.parseInt(sc.nextLine());
        System.out.println("Enter number of mappers : ");
        int mapN = Integer.parseInt(sc.nextLine());
        System.out.println("Enter limit : ");
        int l = Integer.parseInt(sc.nextLine());
        ReduceWorker rw = new ReduceWorker(l, cport, mport, mapN);
         */
        ReduceWorker rw = new ReduceWorker(10, 5000, 4000, 1);
        while (true) {
            rw.waitForTasksThread();
            rw.close();
        }
    }

    private class ClientWatcher implements Runnable {

        private final ServerSocket serverSocket;
        private Socket socket;

        @Override
        public void run() {
            try {
                this.socket = serverSocket.accept();
                waitForMasterAck();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

        public ClientWatcher(ServerSocket serverSocket) {
            this.serverSocket = serverSocket;

        }

        public void waitForMasterAck() {

            try {

                System.out.println("Waiting for ACK");
                cin = new ObjectInputStream(socket.getInputStream());
                cout = new ObjectOutputStream(socket.getOutputStream());
                if (cin.readUTF().equalsIgnoreCase("start")) {
                    System.out.println("Got ACK from master");
                    reduce();
                    sendResults();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
