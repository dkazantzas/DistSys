package distsys;


import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AndroidClient {

    Socket reducer;
    private int n;
    private double lat_min;
    private double long_min;
    private double lat_max;
    private double long_max;
    private Timestamp t_min;
    private Timestamp t_max;

    private final String[] ips_mapper;
    private final String ip_reducer;
    private final int port_mapper;
    private final int port_reducer;
    private Map<LL, Integer> map;
    private final Socket[] sockets_mapper;
    private ObjectInputStream in[];
    private ObjectOutputStream out[];
    private ObjectInputStream inl;
    
    public AndroidClient(int n, String[] ips_mapper, String ip_reducer, int port_mapper, int port_reducer) {
        this.n = n;
        this.ips_mapper = ips_mapper;
        this.ip_reducer = ip_reducer;
        this.port_mapper = port_mapper;
        this.port_reducer = port_reducer;

        sockets_mapper = new Socket[n];
    }

    public void prepareRequest(double lat_min, double long_min, double lat_max, double long_max, Timestamp t_min,
            Timestamp t_max) {
        this.lat_min = lat_min;
        this.long_min = long_min;
        this.lat_max = lat_max;
        this.long_max = long_max;
        this.t_min = t_min;
        this.t_max = t_max;
    }

    public void distributeToMappers() {
        double dx = Math.abs(lat_max - lat_min) / n;

        System.out.println("Distributing to mappers!!! ");
        in = new ObjectInputStream[n];
        out = new ObjectOutputStream[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Distributing to mapper " + i + " !!! ");

            double mapper_lat_min = lat_min + i * dx;
            double mapper_lat_max = lat_min + (i + 1) * dx;

            System.out.println("Sending request to mapper #" + i);
            MapTask task;
            task = new MapTask(mapper_lat_min, long_min, mapper_lat_max, long_max, t_min,
                    t_max);
            System.out.println(task.toString());
            try {
                sockets_mapper[i] = new Socket(InetAddress.getByName(ips_mapper[i]), port_mapper);
                out[i] = new ObjectOutputStream(sockets_mapper[i].getOutputStream());
                out[i].writeObject(task);
                out[i].flush();

            } catch (UnknownHostException uhe) {
                uhe.printStackTrace();
                System.exit(1);
            } catch (IOException ioe) {
                ioe.printStackTrace();
                System.exit(1);
            }
        }
    }

    public void waitForMappers() {

        System.out.println("Waiting for mappers!!! ");
        Thread t[] = new Thread[n];
        for (int i = 0; i < n; i++) {
            try {
                in[i] = new ObjectInputStream(sockets_mapper[i].getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(AndroidClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            t[i] = new Thread(new MapWatcher(in[i]));
            t[i].start();
        }

        for (int i = 0; i < n; i++) {
            try {
                t[i].join();
                System.out.println("Join " + i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // ------------------------------------------------------------------
    public void ackToReducer() {
        try {
            System.out.println("Sending ACK to Reducer");
            reducer = new Socket(ip_reducer, port_reducer);
            ObjectOutputStream outl = new ObjectOutputStream(reducer.getOutputStream());
            inl = new ObjectInputStream(reducer.getInputStream());
            outl.writeUTF("start");
            System.out.println("Sent Ack");
            outl.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
    public void collectDataFromReducers() {
        try {
            
            map = new HashMap<LL,Integer>();
            try {
                map = (Map<LL,Integer>) inl.readObject();
                System.err.println("Received Results");
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(1);
            }
            inl.close();
            reducer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);

        }
    }

    void execute() {
        distributeToMappers();
        waitForMappers();
        ackToReducer();
        collectDataFromReducers();
        print();
    }

    public void print() {
        map.keySet().stream().forEach((ll) -> {
            System.out.println(ll.toString() + ": " + map.get(ll));
        });
    }

    private class MapWatcher implements Runnable {

        private final ObjectInputStream in;

        public MapWatcher(ObjectInputStream in) {
            this.in = in;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (in.readInt() == 1) {
                        break;
                    }
                } catch (IOException e) {

                }
            }

        }
    }
    
}
