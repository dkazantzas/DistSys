package distsys;

import java.io.Serializable;
//  LL stands for Latitude Longitude
public class LL implements Serializable{

    private static final long serialVersionUID = 6246L;
    
    private final double Lat,Long;

    public LL(double Lat, double Long) {
        this.Lat = Lat;
        this.Long = Long;

    }

    public double getLat() {
        return Lat;
    }

    public double getLong() {
        return Long;
    }

    @Override
    public String toString() {
        return "LL{" + "Lat=" + Lat + ", Long=" + Long + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.Lat) ^ (Double.doubleToLongBits(this.Lat) >>> 32));
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.Long) ^ (Double.doubleToLongBits(this.Long) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LL other = (LL) obj;
        if (Double.doubleToLongBits(this.Lat) != Double.doubleToLongBits(other.Lat)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Long) != Double.doubleToLongBits(other.Long)) {
            return false;
        }
        return true;
    }

    

    
}
    

